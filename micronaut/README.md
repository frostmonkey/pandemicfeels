# Pandemicfeels

## Micronaut

https://micronaut.io/

Pohja luotiin komennolla:

```bash
mn create-app --build maven --features graalvm,jdbc-hikari,postgres,logback,data-jpa --lang java --test junit pandemicfeels
```

Eri konfiguraatiomahdollisuudet luontiin löytyvät täältä: https://micronaut-projects.github.io/micronaut-starter/latest/guide/#commands

## Helidon

https://helidon.io

Pohja luontiin komennolla:

```bash
mvn -U archetype:generate -DinteractiveMode=false \
    -DarchetypeGroupId=io.helidon.archetypes \
    -DarchetypeArtifactId=helidon-quickstart-se \
    -DarchetypeVersion=2.1.0 \
    -DgroupId=com.frostmonkey \
    -DartifactId=pandemicfeels \
    -Dpackage=com.frostmonkey.pandemicfeels
```

## Quarkus

https://quarkus.io/

## Yleistä

Testikannan saa käyntiin komennolla:

```bash
docker run -e POSTGRES_USER=pandemicfeels -e POSTGRES_PASSWORD=pandemicfeels -p 5432:5432 postgres:12
```
