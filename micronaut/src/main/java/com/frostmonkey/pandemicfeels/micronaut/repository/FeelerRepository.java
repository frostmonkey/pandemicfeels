package com.frostmonkey.pandemicfeels.micronaut.repository;

import io.micronaut.data.annotation.Repository;
import io.micronaut.data.repository.CrudRepository;

import java.util.UUID;

@Repository
public interface FeelerRepository extends CrudRepository<Feeler, UUID> {
}
