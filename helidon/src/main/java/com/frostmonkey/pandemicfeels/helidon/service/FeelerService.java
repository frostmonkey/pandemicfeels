package com.frostmonkey.pandemicfeels.helidon.service;

import com.frostmonkey.pandemicfeels.helidon.dto.FeelerDTO;
import io.helidon.common.http.Http;
import io.helidon.common.reactive.Subscribable;
import io.helidon.dbclient.DbClient;
import io.helidon.dbclient.DbRow;
import io.helidon.webserver.*;

import javax.json.Json;
import javax.json.JsonObject;
import java.util.UUID;
import java.util.concurrent.CompletionException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FeelerService implements Service {
    private static final Logger LOGGER = Logger.getLogger(FeelerService.class.getName());

    private final DbClient dbClient;

    public FeelerService(DbClient dbClient) {
        this.dbClient = dbClient;
    }

    @Override
    public void update(Routing.Rules rules) {
        rules
            .post("/", Handler.create(FeelerDTO.class, this::createFeeler))
            .get("/{uuid}", this::getFeeler)
            .patch("/{uuid}", Handler.create(FeelerDTO.class, this::updateFeeler))
            .get("/{uuid}/feelings", this::getFeelerFeelings);
    }

    private void getFeelerFeelings(ServerRequest request, ServerResponse response) {

    }

    private void updateFeeler(ServerRequest request, ServerResponse response, FeelerDTO feeler) {
        UUID uuid = UUID.fromString(request.path().param("uuid"));

        dbClient.execute(exec -> exec
                .createNamedInsert("update-feeler")
                .addParam("username", feeler.getUsername())
                .addParam("email", feeler.getEmail())
                .addParam("password", feeler.getPassword())
                .addParam("id", uuid)
                .execute())
                .thenAccept(count -> sendJsonMessage("Updated " + count + " feelers", response))
                .exceptionally(throwable -> sendError(throwable, response));
    }

    private void getFeeler(ServerRequest request, ServerResponse response) {
        UUID uuid = UUID.fromString(request.path().param("uuid"));
        dbClient.execute(exec -> exec
                .createNamedGet("select-feeler")
                .addParam("uuid", uuid)
                .execute())
                .thenAccept(opt -> opt.ifPresentOrElse(
                        it -> sendJsonRow(it, response),
                        () -> sendNotFound(response, "Feeler not found.")))
                .exceptionally(throwable -> sendError(throwable, response));
    }

    public void createFeeler(ServerRequest request, ServerResponse response, FeelerDTO feeler) {
        dbClient.execute(exec -> exec
                .createNamedInsert("insert-feeler")
                .addParam("username", feeler.getUsername())
                .addParam("email", feeler.getEmail())
                .addParam("password", feeler.getPassword())
                .execute())
                .thenAccept(count -> sendJsonMessage("Inserted " + count + " feelers", response))
                .exceptionally(throwable -> sendError(throwable, response));
    }

    private <T> T sendError(Throwable throwable, ServerResponse response) {
        Throwable realCause = throwable;
        if (throwable instanceof CompletionException) {
            realCause = throwable.getCause();
        }
        response.status(Http.Status.INTERNAL_SERVER_ERROR_500);
        response.send("Failed to process request: " + realCause.getClass().getName() + "(" + realCause.getMessage() + ")");
        LOGGER.log(Level.WARNING, "Failed to process request", throwable);
        return null;
    }

    private void sendNotFound(ServerResponse response, String message) {
        response.status(Http.Status.NOT_FOUND_404);
        response.send(message);
    }

    private void sendJsonMessage(String message, ServerResponse response) {
        JsonObject json = Json.createObjectBuilder()
                .add("message", message)
                .build();
        response.send(json);
    }

    private void sendJsonRow(DbRow row, ServerResponse response) {
        // This should be done with a mapper
        JsonObject json = Json.createObjectBuilder()
                .add("username", (String) row.column("username").value())
                .add("email", (String) row.column("email").value())
                .build();
        response.send(json);
    }
}
